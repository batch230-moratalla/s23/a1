console.log("Jopay, kumusta ka na daw?")

/*
	3. Create a trainer object using object literals.
	4. Initialize/add the following trainer object properties:
	a. Name (String)
	b. Age (Number)
	c. Pokemon (Array)
	d. Friends (Object with Array values for properties)
	5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
	6. Access the trainer object properties using dot and square bracket notation.
	7. Invoke/call the trainer talk object method.
*/

let trainer = {
    name: "Ash Ketchum",
    age: "10",
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {
      hoenn: ["May", "Max"],
      Kanto: ["Brock", "Misty"]
    },
    talk: function(){
        console.log(this.pokemon[0] + "!" + " I choose you!" )
    }
}
console.log(trainer)
console.log("Result of dot notation");
console.log(trainer.name);

console.log("Result of square bracket");
console.log(trainer["pokemon"]);

console.log("Result of talk Method");
trainer.talk();

function Pokemon(name, level){
  // Properties
  this.name = name;
  this.level = level;
  this.health = level*2;
  this.attack = level;

  // Method
  // "target" parameter represents another pokemon object
  this.tackle = function(target){
    console.log(this.name + ' tackled ' + target.name);
    target.health -= this.attack;
    console.log(target.name + " health is now reduced to " + target.health)
    if(target.health <= 0){ 
      // Invoke the faint method from the target object if the health is less than or equal to 0
      target.faint()

      if(target.health <= 0){ 
        // Invoke the faint method from the target object if the health is less than or equal to 0
        target.faint()
    }
  }

  } // for free time, try to run faint method using "if statement"
  this.faint = function(){
    console.log(this.name + " fainted");
  }
}
let pikachu = new Pokemon("Pikachu ", 12);
console.log(pikachu);

let geodude = new Pokemon("Geodue", 8);
console.log(geodude);

let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);

geodude.tackle(pikachu);
mewtwo.tackle(geodude);



